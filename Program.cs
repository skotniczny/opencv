﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Diagnostics;
using System.Drawing;

namespace OpenCVTest1
{

    

    class Program
    {

        static void Main(string[] args)
        {
            //otwórz kamerkę (czasem trochę trwa)
            VideoCapture cap = new VideoCapture();
            Mat img = new Mat(); //Obiekt do którego będziemy pobierać klatki
            Mat outimg = new Mat();
            Mat outimg2 = new Mat();
            while (true)
            {
                cap.Read(img); //Pobierz klatkę z kamerki

                CvInvoke.CvtColor(img, outimg, ColorConversion.Bgr2Gray);
                CvInvoke.Sobel(img, outimg, DepthType.Cv16S, 0, 1, 3);
                //Konwersja do Cv8U
                CvInvoke.ConvertScaleAbs(outimg, outimg2, 1, 0);
  
                CvInvoke.Imshow("OpenCV", outimg2); //Wyświetl klatkę

                if ((CvInvoke.WaitKey(1) & 0xff) == 27) break; //ESC- koniec
            }
            CvInvoke.DestroyWindow("OpenCV");
            cap.Dispose();
        }
    }
}
